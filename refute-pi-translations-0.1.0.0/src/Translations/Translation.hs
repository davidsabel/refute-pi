module Translations.Translation where

import AbstractCHPrograms
import AbstractPiPrograms
import Data.List(delete,(\\),permutations,nub,sort,insert)
import System.Environment
import AbstractCHPrograms
import AbstractPiPrograms


data Translation = 
             TrSendPut
           | TrSendTake 
           | TrCheckPut String 
           | TrCheckTake String
    deriving (Eq,Ord)
 
instance Show Translation where
    show (TrSendPut) = "putS"
    show (TrSendTake) ="takeS"
    show (TrCheckPut i) = "putCh^" ++  i
    show (TrCheckTake i) = "takeCh^" ++  i
    
    
type SenderProgram   = [Translation]
type ReceiverProgram = [Translation]

type TranslationCandidate = (SenderProgram,ReceiverProgram)
    
checkTestSet xs = 
    map  (\trans -> map (\(prog,m,s) -> (makeCHProg trans prog,m,s))  testSet) xs
    
    
--
-- makeCHProgr take a TranslationCandidate and an abstract pi program 
-- and generates an abstract CH program 
-- 
makeCHProg :: (SenderProgram, ReceiverProgram) ->  PiProgram -> AbstractCHProgram
makeCHProg translation threads = 
    map  (\thread -> makeCHProgThread translation thread) threads 
    
makeCHProgThread translation thread = 
    concatMap (\cmd ->  makeCHProgCmd translation cmd) thread 

makeCHProgCmd translation (Output x y)  = translateCmd x y (fst translation)    
makeCHProgCmd translation (Input x y u) = translateCmd x y (snd translation)   
    
makeCHProgCmd translation Stop  =      [CHStop]
translateCmd x y [] = []
translateCmd x y (transPiece:rest) =  case transPiece of
    TrSendPut     -> (PutName x y):(translateCmd x y rest)
    TrSendTake     -> (TakeName x y (length rest)):(translateCmd x y rest)
    TrCheckPut s   -> (PutUnit x ("_check_" ++ s)):(translateCmd x y rest)
    TrCheckTake s  -> (TakeUnit x ("_check_" ++ s)):(translateCmd x y rest)
        
 
checkConvergence prog may should = 
     let endZustand = evaluateProgram prog 
     in (isMayConvergent endZustand) == may && (isShouldConvergent endZustand) == should

checkConvergenceTransAll transProglst = all (\(prog,m,s) -> checkConvergence prog m s) transProglst

checkPrograms ps = filter checkConvergenceTransAll (checkTestSet ps)



select [] xs = []
select (y:ys) xs = (xs !! y) : select ys xs

--   ????  variations / combinations?
perm :: [a] -> [[a]]
perm []     =  [[]]                      --  return []
perm (x:xs) = concat [ins x y | y <- (perm xs)]  ---     (perm xs) >>= (ins x)
    where
    ins :: a -> [a] -> [[a]]
    ins x []     = [[x]]
    ins x (y:ys) = [x:y:ys] ++ ( map (y:) (ins x ys) )


insertAll y []     = [[y]]
insertAll y (x:xs) = [(y:x:xs)] ++ (map (x :)  (insertAll y xs))        


multidelete [] ys = ys
multidelete (x:xs) ys = multidelete xs (Data.List.delete x ys) 
