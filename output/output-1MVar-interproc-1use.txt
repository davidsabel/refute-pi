Configuration: 4 with 1 MVars and 1 uses. shortoutput: True
> testing translation candidate 0 :
([takeCh^1,putS],[putCh^1,takeS])
>>> testing pi program 0
([[Output "x" "y",Input "x" "y" 0,Stop]],False,False)
>>> testing pi program 1
([[Output "x" "y",Stop],[Input "x" "y" 0]],True,True)
>>> testing pi program 2
([[Output "x" "y"],[Input "x" "y" 0,Stop]],True,True)
>>> testing pi program 3
([[Input "x" "y" 0,Stop]],False,False)
>>> testing pi program 4
([[Output "x" "y",Stop]],False,False)
>>> testing pi program 5
([[Output "x" "y",Stop],[Output "xx" "yy"],[Input "x" "y" 0]],True,True)
>>> testing pi program 6
([[Output "x" "y"],[Output "xx" "yy",Stop],[Input "x" "y" 0]],False,False)
>>> testing pi program 7
([[Output "x" "y",Stop],[Input "x" "y" 0],[Input "xxx" "yyyy" 0]],True,True)
>>> testing pi program 8
([[Output "x" "y",Input "x" "z" 0,Stop],[Input "x" "w" 0]],False,False)
ok
> testing translation candidate 1 :
([takeCh^1,putS],[takeS,putCh^1])
>>> testing pi program 0
([[Output "x" "y",Input "x" "y" 0,Stop]],False,False)
>>> testing pi program 1
([[Output "x" "y",Stop],[Input "x" "y" 0]],True,True)
ok
> testing translation candidate 2 :
([putS,takeCh^1],[putCh^1,takeS])
>>> testing pi program 0
([[Output "x" "y",Input "x" "y" 0,Stop]],False,False)
>>> testing pi program 1
([[Output "x" "y",Stop],[Input "x" "y" 0]],True,True)
>>> testing pi program 2
([[Output "x" "y"],[Input "x" "y" 0,Stop]],True,True)
>>> testing pi program 3
([[Input "x" "y" 0,Stop]],False,False)
>>> testing pi program 4
([[Output "x" "y",Stop]],False,False)
>>> testing pi program 5
([[Output "x" "y",Stop],[Output "xx" "yy"],[Input "x" "y" 0]],True,True)
>>> testing pi program 6
([[Output "x" "y"],[Output "xx" "yy",Stop],[Input "x" "y" 0]],False,False)
>>> testing pi program 7
([[Output "x" "y",Stop],[Input "x" "y" 0],[Input "xxx" "yyyy" 0]],True,True)
>>> testing pi program 8
([[Output "x" "y",Input "x" "z" 0,Stop],[Input "x" "w" 0]],False,False)
ok
> testing translation candidate 3 :
([putS,takeCh^1],[takeS,putCh^1])
>>> testing pi program 0
([[Output "x" "y",Input "x" "y" 0,Stop]],False,False)
>>> testing pi program 1
([[Output "x" "y",Stop],[Input "x" "y" 0]],True,True)
>>> testing pi program 2
([[Output "x" "y"],[Input "x" "y" 0,Stop]],True,True)
>>> testing pi program 3
([[Input "x" "y" 0,Stop]],False,False)
>>> testing pi program 4
([[Output "x" "y",Stop]],False,False)
>>> testing pi program 5
([[Output "x" "y",Stop],[Output "xx" "yy"],[Input "x" "y" 0]],True,True)
>>> testing pi program 6
([[Output "x" "y"],[Output "xx" "yy",Stop],[Input "x" "y" 0]],False,False)
>>> testing pi program 7
([[Output "x" "y",Stop],[Input "x" "y" 0],[Input "xxx" "yyyy" 0]],True,True)
>>> testing pi program 8
([[Output "x" "y",Input "x" "z" 0,Stop],[Input "x" "w" 0]],False,False)
>>> testing pi program 9
([[Output "x" "y",Output "x" "z",Stop],[Input "x" "w" 0]],False,False)
>>> testing pi program 10
([[Input "x" "y" 0,Input "x" "z" 0,Stop],[Output "x" "w",Input "w" "z" 0]],False,False)
>>> testing pi program 11
([[Output "x" "z",Output "z" "a",Stop],[Output "x" "w",Output "w" "a",Stop],[Input "x" "y" 0,Input "y" "u" 0]],True,True)
ok
> testing translation candidate 4 :
([putCh^1,putS],[takeCh^1,takeS])
>>> testing pi program 0
([[Output "x" "y",Input "x" "y" 0,Stop]],False,False)
ok
> testing translation candidate 5 :
([putCh^1,putS],[takeS,takeCh^1])
>>> testing pi program 0
([[Output "x" "y",Input "x" "y" 0,Stop]],False,False)
ok
> testing translation candidate 6 :
([putS,putCh^1],[takeCh^1,takeS])
>>> testing pi program 0
([[Output "x" "y",Input "x" "y" 0,Stop]],False,False)
ok
> testing translation candidate 7 :
([putS,putCh^1],[takeS,takeCh^1])
>>> testing pi program 0
([[Output "x" "y",Input "x" "y" 0,Stop]],False,False)
ok
> done!
==================================================================
  Summary
==================================================================

Used counterexamples: [0,1,8,11]
Number of candidates: 8
