module Main where
-- -------------------------------------
import AbstractCHPrograms
import AbstractPiPrograms
import Translations
-- -------------------------------------
import Translations.Comb
import Translations.Translation
-- -------------------------------------
import System.Environment
import System.Timeout
import Control.Exception
import Data.IORef
import Data.List(insert,nub,sort)
--import Data.List(insert,nub,sort,(\\))
import Text.Read 
    
-- main = mainComb4W
   
shortloop = 
    do
        putStrLn "Short output (Y/N)?"
        inputshort <- getLine
        case inputshort of 
             "Y" -> return True
             "N" -> return False
             _ -> shortloop

main = do 
        arg <- getArgs
        let output str = if ((length arg) > 0) &&
                            ((head arg) == "-o") && (length arg) == 2 then appendFile (arg!!1) (str ++ "\n") else putStrLn str
        if ((length arg) > 0) &&((head arg) == "-o") && (length arg) == 2 then writeFile (arg!!1) "" else return ()
        let loop = do 
                    putStrLn "Choose test to run"
                    putStrLn "(1) No acknowledgement MVar"
                    putStrLn "(2) i acknowledgement MVars, no reuse, interprocess communication only"
                    putStrLn "(3) i acknowledgement MVars, j uses"
                    putStrLn "(4) i acknowledgement MVars, j uses, interprocess communication only"                    
                    putStrLn "(q) Exit"
                    input <- getLine
                    case input of
                      "q" -> return ()
                      "1" -> do 
                              short <- shortloop 
                              output ("Configuration: 0 with short: "  ++ show short)
                              mainComb0 short output
                      "2" -> output "Configuration: 2" >>
                              do
                               let loop2 = do 
                                           putStrLn "number of MVars?"
                                           inputi <- getLine
                                           case readMaybe inputi of 
                                             Just num -> do 
                                                          short <- shortloop
                                                          output ("Configuration: 2 with " ++ show num ++ " MVars" ++ " shortoutput: " ++ (show short))  >> mainCombi_interproc_only_no_reuse short output num
                                             other -> do
                                                       putStrLn "insert valid number"
                                                       loop2
                               loop2
                      "3" -> do
                              let loop2 = do 
                                           putStrLn "number of MVars?"
                                           inputi <- getLine
                                           case readMaybe inputi of 
                                             Just num -> do 
                                                            putStrLn "number of uses?"
                                                            inputj <- getLine
                                                            case readMaybe inputj of 
                                                                Just uses -> do
                                                                                short <- shortloop
                                                                                output ("Configuration: 3 with " ++ show num ++ " MVars and " ++ show uses ++ " uses" ++ " with shortoutput: " ++ show short) 
                                                                                mainCombi short output num uses
                                                                other -> do putStrLn "insert valid number"
                                                                            loop2
                                             other -> do putStrLn "insert valid number"
                                                         loop2
                              loop2
                      "4" -> do
                              let loop2 = do 
                                           putStrLn "number of MVars?"
                                           inputi <- getLine
                                           case readMaybe inputi of 
                                             Just num -> do 
                                                            putStrLn "number of uses?"
                                                            inputj <- getLine
                                                            case readMaybe inputj of 
                                                                Just uses -> do
                                                                                short <- shortloop
                                                                                output ("Configuration: 4 with " ++ show num ++ " MVars and " ++ show uses ++ " uses. shortoutput: " ++ show short)
                                                                                mainCombiSymm short output num uses
                                                                other -> do putStrLn "insert valid number"
                                                                            loop2
                                             other -> do putStrLn "insert valid number"
                                                         loop2
                              loop2                              
                                                           
                      _ -> loop
        loop
                      
                        
  
-- 
-- No acknowledgement MVars at all
-- 
mainComb0 short output =     
      let  
        -- -------------------------------------
        -- Configuration
        -- -------------------------------------        
        -- set of translations to be tested:
        translationSet = distributeActions []
        -- translations that should be skipped:
        skipSet ::[Int]
        skipSet = []  -- usually leave empty, the main program will collect the them by timeout
        -- timeout value 
        timeOutVal = 10*1000000 -- 10 Seconds
        -- set the counter examples (usually modify testSet in Module AbstractPiPrograms)
        piProg  = if testConsistency then testSet else error "consistency of pi programs is violated!!!"
       in run short output translationSet skipSet timeOutVal piProg
       -- -------------------------------------                          

-- 
-- 1 acknowledgement MVars 
-- 

-- 
-- only interprocess acknowledgement, no reuse
-- 
   
mainComb1_interproc_only_no_reuse short output =
      let 
        -- -------------------------------------
        -- Configuration
        -- -------------------------------------        
        -- set of translations to be tested:
        translationSet = distributeActionsWeighted [TrCheckTake "1",  TrCheckPut "1"] 
        -- translations that should be skipped:
        skipSet ::[Int]
        skipSet = []  -- usually leave empty, the main program will collect the them by timeout
        -- timeout value 
        timeOutVal = 10*1000000 -- 10 Seconds
        -- set the counter examples (usually modify testSet in Module AbstractPiPrograms)
        piProg  = if testConsistency then testSet else error "consistency of pi programs is violated!!!"

      in run short output translationSet skipSet timeOutVal piProg
       -- -------------------------------------                          
-- 
--  no reuse
-- 
   

mainCombi_interproc_only_no_reuse short output  num =
      let 
        -- -------------------------------------
        -- Configuration
        -- -------------------------------------        
        -- set of translations to be tested:
        translationSet = distributeActionsSymm 1 (concat [[TrCheckTake (show i),  TrCheckPut (show i)] | i <- [1..num]])
        -- translations that should be skipped:
        skipSet ::[Int]
        skipSet = []  -- usually leave empty, the main program will collect the them by timeout
        -- timeout value 
        timeOutVal = 10*1000000 -- 10 Seconds
        -- set the counter examples (usually modify testSet in Module AbstraftectPiPrograms)
        piProg  = if  (and $ map checkTest testSet3) then testSet3 else error "consistency of pi programs is violated!!!"

      in run short output translationSet skipSet timeOutVal piProg
       -- -------------------------------------                           
   

mainCombi short output  num uses=
      let 
        -- -------------------------------------
        -- Configuration
        -- -------------------------------------        
        -- set of translations to be tested:
        translationSet = distributeActions (concat (concat [ [ [TrCheckTake (show i),  TrCheckPut (show i)] | j <- [1..uses]] | i <- [1..num]]))
        -- translations that should be skipped:
        skipSet ::[Int]
        skipSet = []  -- usually leave empty, the main program will collect the them by timeout
        -- timeout value 
        timeOutVal = 10*1000000 -- 10 Seconds
        -- set the counter examples (usually modify testSet in Module AbstractPiPrograms)
        piProg  = if testConsistency then testSet else error "consistency of pi programs is violated!!!"

      in run short output translationSet skipSet timeOutVal piProg
       -- -------------------------------------                     

test  =
      let
        -- -------------------------------------
        -- Configuration
        -- -------------------------------------        
        -- set of translations to be tested:
        translationSet = [([TrSendPut, TrCheckPut "1", TrCheckPut "1", TrCheckTake "1"],[TrSendTake, TrCheckTake "1"])]
        -- translations that should be skipped:
        skipSet ::[Int]
        skipSet = []  -- usually leave empty, the main program will collect the them by timeout
        -- timeout value 
        timeOutVal = 10*1000000 -- 10 Seconds
        -- set the counter examples (usually modify testSet in Module AbstractPiPrograms)
        piProg  = if testConsistency then testSet else error "consistency of pi programs is violated!!!"

      in run False putStrLn translationSet skipSet timeOutVal piProg
       -- -------------------------------------   

       
gset = [((a ++ [TrSendPut] ++ b),(c ++ [TrSendTake] ++ d)) | 
     a <- [[],[TrCheckPut "1"],[TrCheckTake "1"]]
     ,b <- [[],[TrCheckPut "1"],[TrCheckTake "1"]]
     , c <- [[],[TrCheckPut "1"],[TrCheckTake "1"]]
     , d <- [[],[TrCheckPut "1"],[TrCheckTake "1"]]
     ]

gset2 = [((a ++ [TrSendPut] ++ b),(c ++ [TrSendTake] ++ d)) | 
     a <- [[],[TrCheckPut "1"],[TrCheckTake "1"]]
     ,b <- [[],[TrCheckPut "1",TrCheckPut "1",TrCheckTake "1"],[TrCheckTake "1"]]
     , c <- [[],[TrCheckPut "1"],[TrCheckTake "1"]]
     , d <- [[],[TrCheckTake "1"],[TrCheckPut "1",TrCheckPut "1",TrCheckTake "1"]]
     ]

testSet3 = 
            [
              ([[Output "x" "y",Stop],[Input "x" "y" 0]],True,True),
             ([[Output "x" "y",Input "x" "z" 0,Output "z" "q"],[Input "x" "z" 0],[Input "x" "z" 0],[Output "x" "z"],[Input "y" "u" 0,Stop]],False,False)]
                
                 
-- where two processes suffice to refute all translation variants:
-- $\outputxy{x}{y}.\tStop \PAR \inputxy{x}{y}$  and   
-- %%([[Put "x" "y",Stop],[Take "x" "y" 0]],True,True)
-- %%*Main> pruefProgs!!29

-- -- %% ([[Output "x" "y",Input "x" "z" 0,Output "z" "q"],[Input "x" "z" 0],[Input "x" "z" 0],[Output "x" "z"],[Input "x" "u" 0,Stop]],False,False)
-- $\outputxy{x}{y}.\inputxy{x}{z}.\outputxy{z}{q} \PAR \inputxy{x}{z} \PAR \inputxy{x}{z}\PAR \outputxy{x}{z}\PAR \inputxy{x}{u}.\tStop$,                  
     
testSet2 = filter (\(x,y,z) -> not y || z )    --  not may-convergente oder should-convergent
           [([[Output "x" "y",Input "x" "y" 0,Stop]],False,False),
             ([[Output "x" "y",Stop], [Input "x" "y" 0]],True,True),
             ([[Output "x" "y"], [Input "x" "y" 0,Stop]],True,True),
      --         ([[Output "x" "y"], [Input "x" "y" 0,Stop],[Output "xx" "yy"], [Input "xx" "yy" 0]],True,True),
             ([[Input "x" "y" 0,Stop]],False,False),
             ([[Output "x" "y",Stop]],False,False),
             ([[Output "x" "y",Stop],[Output "xx"  "yy"],[Input "x" "y" 0]],True,True),
              ([[Output "x" "y"],[Output "xx"  "yy",Stop],[Input "x" "y" 0]],False,False),
             ([[Output "x" "y",Stop],[Input "x" "y" 0],[Input "xxx" "yyyy" 0]],True,True),
             --- P =  ('xy.x(z).stop | x(w).0)
               ([[Output "x" "y", Input "x" "z" 0, Stop],  [Input "x" "w" 0]],False,False),
               --Neu wegen TT:  
               --- P =  ('xy.'xz.stop | x(w).0)  und symmetrische
               ([[Output "x" "y", Output "x" "z", Stop],  [Input "x" "w" 0]],False,False),
        --       ([[Output "x" "y", Output "x" "z", Output "z" "zz", Stop],  [Input "x" "w" 0,Input "x" "w" 0]],False,False),
               ([[Input "x" "y" 0, Input "x" "z" 0, Stop],  [Output "x" "w", Input "w" "z" 0]],False,False),
             --- P =  ('xz.'za.stop | 'xw.'wa.stop | x(y).y(_).0)
             ([[Output "x" "z", Output "z" "a", Stop], 
               [Output "x" "w", Output "w" "a", Stop],
               [Input "x" "y" 0, Input "y" "u" 0]],True,True),
                 --  P =  ('xz.'za |  x(w).w(b).stop | x(y).'xy.0)
              ([[Output "x" "z", Output "z" "a"], 
               [Input "x" "w" 0, Input "w" "b" 0, Stop],
               [Input "x" "y" 0, Output "x" "y"]],True,True),
               --- P =  x(z). z(a).Stop | (x(w).w(a)a.Stop | 'x(y).'y(b).0)
              ([[Input "x" "z" 0, Input "z" "a" 0, Stop], 
               [Input "x" "w" 0, Input "w" "a" 0, Stop],
               [Output "x" "y", Output "y" "u"]],True,True) ,
               --  neue Test ans Ende, damit es nicht so lange dauert
                       ([[Output "x" "y"], [Input "x" "y" 0,Stop], [Output "x" "y"]],True,True),
                      ([[Output "x" "y",Output "x" "y"], [Input "x" "y" 0,Stop], [Input "x" "y" 0, Output "x" "y"]],True,True),
                       ([[Output "x" "y"], [Input "x" "y" 0], [Output "x" "y",Stop]],True,False),
                --  P =  ('xy.'xz.'zu. stop | x(w).0 | x(u).0 | 'xu.0)   und symmetrische  
                --  das ist das  --22er das zu lange dauert
               ([[Output "x" "y", Output "x" "z", Output "z" "u", Stop],  [Input "x" "w" 0], [Input "x" "ww" 0], [Output "x" "u"]],False,False),
                 ([[Input "x" "y" 0, Input "x" "z" 0, Input "z" "u" 0, Stop],  [Output "x" "w"], [Output "x" "ww"], [Input "x" "u" 0]],False,False), 
                 ([[Input "x" "y" 0, Input "x" "z" 0, Input "z" "u" 0, Stop],  [Output "x" "w"], [Output "x" "ww"], [Input "x" "u" 0],[Input "x" "u" 0]],False,False), 
                 ([[Input "x" "y" 0, Input "x" "z" 0, Input "z" "u" 0, Stop],  [Output "x" "w"], [Output "x" "ww"], [Output "x" "u"]],False,False),
                 ([[Input "x" "y" 0, Input "x" "z" 0, Input "z" "u" 0, Stop],  [Output "x" "w"], [Output "x" "ww"], [Output "w" "u"]],True,False),  
                 --  verschiedene Wege zum Stop:
                  ([[Input "x" "y" 0, Input "y" "z" 0, Stop],  [Output "x" "w"], [Output "x" "ww"], [Output "w" "u"],[Output "ww" "u"]],True,True),  
                 --  hier zwei Tuerme die sich gegenseitig abarbeiten
             ([[Output "x" "y", Input "y" "z" 0, Stop],  [Input "x" "w" 0, Output "w" "u" ]],True,True),
              ([[Output "x" "y", Input "y" "z" 0, Input "z" "zz" 0],  [Input "x" "w" 0, Output "w" "u" ],[Output "u" "uu",Stop]],True,True),
               ([[Output "x" "y", Input "y" "z" 0, Input "z" "zz" 0], [Input "x" "w"  0], [Input "x" "w" 0, Output "w" "u" ],[Output "u" "uu",Stop]],True,False),
               -- 'xw.x(y).y'(q).0 | x(z).0 | x'(z).0 | w(p).Stop   --  von David
               --   neu dazu:  weiss nicht ob die sinnvoll sind ---
           ([[Output "x" "y"],[Input "x" "y" 0,Stop],[Input "x" "y" 0,Stop]],True,True),
            ([[Output "x" "y",Stop],[Output "x" "y",Stop],[Input "x" "y" 0]],True,True),
             ([[Output "x" "y"],[Output "x" "y"],[Input "x" "y" 0,Stop],[Input "x" "y" 0,Stop]],True,True),
            ([[Output "x" "y",Stop],[Output "x" "y",Stop],[Input "x" "y" 0],[Input "x" "y" 0]],True,True),
            ([[Output "x" "y",Stop],[Output "x" "y",Stop],[Output "x" "y",Stop],[Input "x" "y" 0]],True,True),
              ([[Output "x" "y"],[Input "x" "y" 0,Stop],[Input "x" "y" 0,Stop],[Input "x" "y" 0,Stop]],True,True),
               ([[Output "x" "y",Stop],[Output "x" "y",Stop],[Output "x" "y",Stop],[Input "x" "y" 0]],True,True)
               ,([[Output "x" "w", Input "x" "y" 0, Output "y" "q"]
                ,[Input "x" "z" 0]
                ,[Output "x" "z"]
                ,[Input "w" "p" 0, Stop]
                ],False,False) 
           
      --     fuer den letzten Fall? 
      --    'xy.x(z).'z(q) | x(z)  | x(z) | 'x(z) | y(u).Stop    
                ,([[Output "x" "y", Input "x" "z" 0,   Output "z" "q"]
                ,[Input "x" "z" 0]
                ,[Input "x" "z" 0]
                 ,[Output "x" "z"]
                 ,[Input "y" "u" 0,Stop]
                ],False,False)                
                --   symmetrische Variante: 
                ,([[Input "x" "w" 0, Output "x" "y",   Input "y" "q" 0]
                ,[Input "x" "z" 0]
                ,[Output "x" "z"]
                ,[Output "w" "p", Stop]
                ],False,False) 
                --  das dauert auch zu lange, keine AHnung ob da ne schleife ist:
                  , -- x'z.z'a.p'q.Stop | x'w.w'a.p(_).Stop | x(y).y(u).0 | x(l).l(_).0  (David: Versuch 
                   ([[Output "x" "z", Output "z" "a",Output "p" "q",Stop],  
                     [Output "x" "w", Output "w" "a",Input "p" "r" 0, Stop],
                     [Input "x" "y" 0, Input "y" "u" 0],
                     [Input "x" "l" 0, Input "l" "u" 0]
                    ],True,True)
             ]   

{-
test2  =
      let
        -- -------------------------------------
        -- Configuration
        -- -------------------------------------        
        -- set of translations to be tested:
        translationSet = gset ++ gset2
        --   translationSet = gset2  -- das wr nur um es einzeln zu checken
        -- translations that should be skipped:
        skipSet ::[Int]
        skipSet = []  -- usually leave empty, the main program will collect them by timeout
        -- timeout value 
        timeOutVal = 10*1000000 -- 10 Seconds
        -- set the counter examples (usually modify testSet in Module AbstractPiPrograms)
        piProg  = if (and $ map checkTest testSet2) then reverse testSet2 else error "consistency of pi programs is violated!!!"

      in run False putStrLn translationSet skipSet timeOutVal piProg
 
     --  ghc -O Main
     --
     --  ./Main  
-}
mainCombiSymm short output  num uses=
      let 
        -- -------------------------------------
        -- Configuration
        -- -------------------------------------        
        -- set of translations to be tested:
        translationSet = distributeActionsSymm uses (concat [[TrCheckTake (show i),  TrCheckPut (show i)] | i <- [1..num]])
        -- translations that should be skipped:
        skipSet ::[Int]
        skipSet = []  -- usually leave empty, the main program will collect the them by timeout
        -- timeout value 
        timeOutVal = 10*1000000 -- 10 Seconds
        -- set the counter examples (usually modify testSet in Module AbstractPiPrograms)
        piProg  = if testConsistency then testSet else error "consistency of pi programs is violated!!!"

      in run short output translationSet skipSet timeOutVal piProg
       -- -------------------------------------             
   
mainComb1_no_reuse short output = mainComb1 short output 1
-- 
-- i times using the acknowledgement MVars
--   ( i = 1  == no_reuse)
   
mainComb1 short output i =
      let 
        -- -------------------------------------
        -- Configuration
        -- -------------------------------------        
        -- set of translations to be tested:
        translationSet = distributeActions (concat (replicate i [TrCheckTake "1",  TrCheckPut "1"]))
        -- translations that should be skipped:
        skipSet ::[Int]
        skipSet = []  -- usually leave empty, the main program will collect the them by timeout
        -- timeout value 
        timeOutVal = 10*1000000 -- 10 Seconds
        -- set the counter examples (usually modify testSet in Module AbstractPiPrograms)
        piProg  = if testConsistency then testSet else error "consistency of pi programs is violated!!!"
       in run short output translationSet skipSet timeOutVal piProg
       -- -------------------------------------                          
       
       
mainComb2W short output =
      let 
        -- -------------------------------------
        -- Configuration
        -- -------------------------------------        
        -- set of translations to be tested:
        translationSet = distributeActionsWeighted [TrCheckTake "1",  TrCheckPut "1"] 
        -- translations that should be skipped:
        skipSet ::[Int]
        skipSet = []  -- usually leave empty, the main program will collect the them by timeout
        -- timeout value 
        timeOutVal = 10*1000000 -- 10 Seconds
        -- set the counter examples (usually modify testSet in Module AbstractPiPrograms)
        piProg  = if testConsistency then testSet else error "consistency of pi programs is violated!!!"
       in run short output translationSet skipSet timeOutVal piProg
       -- -------------------------------------                          
       
{- result:
Used counterexamples: [0,1,2,8,11]
-}

mainComb22 short output =
      let 
        -- -------------------------------------
        -- Configuration
        -- -------------------------------------        
        -- set of translations to be tested:
        translationSet = distributeActions [TrCheckTake "1",  TrCheckPut "1",TrCheckTake "1",  TrCheckPut "1"] 
        -- translations that should be skipped:
        skipSet ::[Int]
        skipSet = []  -- usually leave empty, the main program will collect the them by timeout
        -- timeout value 
        timeOutVal = 10*1000000 -- 10 Seconds
        -- set the counter examples (usually modify testSet in Module AbstractPiPrograms)
        piProg  = if testConsistency then testSet else error "consistency of pi programs is violated!!!"
       in run short output translationSet skipSet timeOutVal piProg
       -- -------------------------------------         
       
{- result:
Used counterexamples: [0,1,2,8,9,11,12]
-}
 
mainComb23 short output =
      let 
        -- -------------------------------------
        -- Configuration
        -- -------------------------------------        
        -- set of translations to be tested:
        translationSet = distributeActions [TrCheckTake "1"
                                           ,TrCheckPut "1"
                                           ,TrCheckTake "1"
                                           ,TrCheckPut "1"
                                           ,TrCheckTake "1"
                                           ,TrCheckPut "1"] 
        -- translations that should be skipped:
        skipSet ::[Int]
        skipSet = []  -- usually leave empty, the main program will collect the them by timeout
        -- timeout value 
        timeOutVal = 10*1000000 -- 10 Seconds
        -- set the counter examples (usually modify testSet in Module AbstractPiPrograms)
        piProg  = if testConsistency then testSet else error "consistency of pi programs is violated!!!"
       in run short output translationSet skipSet timeOutVal piProg
       -- -------------------------------------                       
{- result:
Used counterexamples: [0,1,2,8,9,11,12]
-}
mainComb24 short output =
      let 
        -- -------------------------------------
        -- Configuration
        -- -------------------------------------        
        -- set of translations to be tested:
        translationSet = distributeActions [TrCheckTake "1"
                                           ,TrCheckPut "1"
                                           ,TrCheckTake "1"
                                           ,TrCheckPut "1"
                                           ,TrCheckTake "1"
                                           ,TrCheckPut "1"
                                           ,TrCheckTake "1"
                                           ,TrCheckPut "1"
                                            ] 
        -- translations that should be skipped:
        skipSet ::[Int]
        skipSet = []  -- usually leave empty, the main program will collect the them by timeout
        -- timeout value 
        timeOutVal = 10*1000000 -- 10 Seconds
        -- set the counter examples (usually modify testSet in Module AbstractPiPrograms)
        piProg  = if testConsistency then testSet else error "consistency of pi programs is violated!!!"
       in run short output translationSet skipSet timeOutVal piProg
       -- -------------------------------------       
{- result:
Used counterexamples: [0,1,2,8,9,11,12]

-}      
mainComb3 short output =
      let 
        -- -------------------------------------
        -- Configuration
        -- -------------------------------------        
        -- set of translations to be tested:
        translationSet = distributeActions [TrCheckTake "1"
                                           ,TrCheckPut "1"
                                           ,TrCheckTake "2"
                                           ,TrCheckPut "2"] 
        -- translations that should be skipped:
        skipSet ::[Int]
        skipSet = []  -- usually leave empty, the main program will collect the them by timeout
        -- timeout value 
        timeOutVal = 20*1000000 -- 10 Seconds
        -- set the counter examples (usually modify testSet in Module AbstractPiPrograms)
        piProg  = if testConsistency then testSet else error "consistency of pi programs is violated!!!"
       in run short output translationSet skipSet timeOutVal piProg
       -- -------------------------------------                          
  
{-
result:
Used counterexamples: [0,1,2,8,11,33,34]
Skipped translations (by timeout): [153,323]
153: ([TrCheckPut "B",TrSendPut,TrCheckTake "A",TrCheckTake "B"],[TrSendTake,TrCheckPut "A"])
323: ([TrCheckTake "B",TrSendPut],[TrCheckPut "A",TrCheckPut "B",TrSendTake,TrCheckTake "A"])
-}
 

mainComb3W short output =
      let
        -- -------------------------------------
        -- Configuration
        -- -------------------------------------
        -- set of translations to be tested:
        translationSet = distributeActionsWeighted
                                           [TrCheckTake "1"
                                           ,TrCheckPut "1"
                                           ,TrCheckTake "2"
                                           ,TrCheckPut "2"]
        -- translations that should be skipped:
        skipSet ::[Int]
        skipSet = []  -- usually leave empty, the main program will collect the them by timeout
        -- timeout value
        timeOutVal = 10*1000000 -- 10 Seconds
        -- set the counter examples (usually modify testSet in Module AbstractPiPrograms)
        piProg  = if testConsistency then testSet else error "consistency of pi programs is violated!!!"
       in run short output translationSet skipSet timeOutVal piProg
       -- -------------------------------------

{-
result:
Used counterexamples: [0,1,2,8,11,33,34]
Skipped translations (by timeout): [153,323]
153: ([TrCheckPut "B",TrSendPut,TrCheckTake "A",TrCheckTake "B"],[TrSendTake,TrCheckPut "A"])
323: ([TrCheckTake "B",TrSendPut],[TrCheckPut "A",TrCheckPut "B",TrSendTake,TrCheckTake "A"])
-}
 
mainComb31 short output =
      let 
        -- -------------------------------------
        -- Configuration
        -- -------------------------------------        
        -- set of translations to be tested:
        translationSet = distributeActions  [TrCheckTake "1",  TrCheckPut "1",TrCheckTake "2",  TrCheckPut "2",TrCheckTake "1",TrCheckPut "1"] 
        -- translations that should be skipped:
        skipSet ::[Int]
        skipSet = []  -- usually leave empty, the main program will collect the them by timeout
        -- timeout value 
        timeOutVal = 40*1000000 -- 10 Seconds
        -- set the counter examples (usually modify testSet in Module AbstractPiPrograms)
        piProg  = if testConsistency then testSet else error "consistency of pi programs is violated!!!"
       in run short output translationSet skipSet timeOutVal piProg
       -- -------------------------------------              
 

mainComb4W short output =
      let 
        -- -------------------------------------
        -- Configuration
        -- -------------------------------------        
        -- set of translations to be tested:
        translationSet = distributeActionsWeighted [TrCheckTake "1",  TrCheckPut "1",TrCheckTake "2",  TrCheckPut "2",TrCheckTake "3",  TrCheckPut "3"] 
        -- translations that should be skipped:
        skipSet ::[Int]
        skipSet = []  -- usually leave empty, the main program will collect the them by timeout
        -- timeout value 
        timeOutVal = 30*1000000 -- 10 Seconds
        -- set the counter examples (usually modify testSet in Module AbstractPiPrograms)
        piProg  = if testConsistency then testSet else error "consistency of pi programs is violated!!!"
       in run short output translationSet skipSet timeOutVal piProg
       -- -------------------------------------                
{-
Used counterexamples: [0,1,2,8,11,33,34]
Skipped translations (by timeout): [635,683,707,1043,1067,1110,1113,1114,1115,1119,1120,1121,1139,1313,1465,1506,1664,1880,1892]
635: ([TrCheckPut "C",TrCheckTake "C",TrCheckTake "B",TrSendPut],[TrCheckPut "A",TrCheckPut "B",TrSendTake,TrCheckTake "A"])
683: ([TrCheckPut "C",TrCheckTake "B",TrCheckTake "C",TrSendPut],[TrCheckPut "A",TrCheckPut "B",TrSendTake,TrCheckTake "A"])
707: ([TrCheckTake "B",TrCheckPut "C",TrCheckTake "C",TrSendPut],[TrCheckPut "A",TrCheckPut "B",TrSendTake,TrCheckTake "A"])
1043: ([TrCheckTake "B",TrSendPut,TrCheckPut "C",TrCheckTake "C"],[TrCheckPut "A",TrCheckPut "B",TrSendTake,TrCheckTake "A"])
1067: ([TrCheckTake "B",TrCheckPut "C",TrSendPut,TrCheckTake "C"],[TrCheckPut "A",TrCheckPut "B",TrSendTake,TrCheckTake "A"])
1110: ([TrCheckPut "C",TrSendPut,TrCheckTake "B",TrCheckTake "C"],[TrSendTake,TrCheckPut "B",TrCheckPut "A",TrCheckTake "A"])
1113: ([TrCheckPut "C",TrSendPut,TrCheckTake "B",TrCheckTake "C"],[TrSendTake,TrCheckPut "A",TrCheckPut "B",TrCheckTake "A"])
1114: ([TrCheckPut "C",TrSendPut,TrCheckTake "B",TrCheckTake "C"],[TrCheckPut "A",TrSendTake,TrCheckPut "B",TrCheckTake "A"])
1115: ([TrCheckPut "C",TrSendPut,TrCheckTake "B",TrCheckTake "C"],[TrCheckPut "A",TrCheckPut "B",TrSendTake,TrCheckTake "A"])
1119: ([TrCheckPut "C",TrSendPut,TrCheckTake "B",TrCheckTake "C"],[TrSendTake,TrCheckPut "A",TrCheckTake "A",TrCheckPut "B"])
1120: ([TrCheckPut "C",TrSendPut,TrCheckTake "B",TrCheckTake "C"],[TrCheckPut "A",TrSendTake,TrCheckTake "A",TrCheckPut "B"])
1121: ([TrCheckPut "C",TrSendPut,TrCheckTake "B",TrCheckTake "C"],[TrCheckPut "A",TrCheckTake "A",TrSendTake,TrCheckPut "B"])
1139: ([TrCheckPut "C",TrCheckTake "B",TrSendPut,TrCheckTake "C"],[TrCheckPut "A",TrCheckPut "B",TrSendTake,TrCheckTake "A"])
1313: ([TrCheckPut "C",TrCheckPut "B",TrCheckTake "A",TrSendPut],[TrCheckTake "B",TrCheckPut "A",TrSendTake,TrCheckTake "C"])
1465: ([TrSendPut,TrCheckPut "B",TrCheckTake "A",TrCheckPut "C"],[TrCheckTake "B",TrCheckPut "A",TrCheckTake "C",TrSendTake])
1506: ([TrCheckPut "B",TrSendPut,TrCheckTake "A",TrCheckPut "C"],[TrSendTake,TrCheckPut "A",TrCheckTake "C",TrCheckTake "B"])
1664: ([TrCheckPut "C",TrCheckTake "B",TrSendPut,TrCheckTake "A"],[TrCheckTake "C",TrCheckPut "B",TrSendTake,TrCheckPut "A"])
1880: ([TrCheckTake "C",TrSendPut,TrCheckTake "B",TrCheckTake "A"],[TrCheckPut "C",TrCheckPut "B",TrSendTake,TrCheckPut "A"])
1892: ([TrCheckTake "C",TrSendPut,TrCheckTake "B",TrCheckTake "A"],[TrCheckPut "A",TrCheckPut "C",TrSendTake,TrCheckPut "B"])

-}
mainComb4 short output =
      let 
        -- -------------------------------------
        -- Configuration
        -- -------------------------------------        
        -- set of translations to be tested:
        translationSet = distributeActions [TrCheckTake "1",  TrCheckPut "1",TrCheckTake "2",  TrCheckPut "2",TrCheckTake "3",  TrCheckPut "3"] 
        -- translations that should be skipped:
        skipSet ::[Int]
        skipSet = []  -- usually leave empty, the main program will collect the them by timeout
        -- timeout value 
        timeOutVal = 10*1000000 -- 10 Seconds
        -- set the counter examples (usually modify testSet in Module AbstractPiPrograms)
        piProg  = if testConsistency then testSet else error "consistency of pi programs is violated!!!"
       in run short output translationSet skipSet timeOutVal piProg
       -- -------------------------------------                
  


--
-- the main function, iteratively tries to reject the correctness of the translation
-- 


--
-- the parametrized run function

run shortoutput output translationSet skipSet timeOutVal piProg = 
  do
    counterex <- newIORef ([],[])
    loop counterex 0 
    where loop c i 
            | i `elem` skipSet = do 
                                   output ("> testing translation candidate " ++ show i)
                                   output (show $ translationSet!!i)
                                   output "skipped"
                                   loop c (i+1)
            | i >= (length translationSet) = do 
                                               output "> done!"
                                               (con,sk) <- readIORef c
                                               output "=================================================================="                                               
                                               output "  Summary"
                                               output "=================================================================="                                               
                                               if shortoutput then return () 
                                                else do 
                                                   mapM_ output ["\nTranslation candidate " 
                                                                ++ show ti ++ ": \n   "
                                                                ++ show (translationSet!!ti) 
                                                                ++ "\n is violated by pi-program with number " ++ show pn ++ ":\n   " 
                                                                ++ showPiProgram piprog
                                                                ++ "\n which "
                                                                ++ (if maypi then "is  may convergent     " else "is  not may convergent " )
                                                                ++ "and" 
                                                                ++ (if shouldpi then " should convergent" else " not should convergent")
                                                                ++ ".\n"
                                                                ++ " The translation is \n   " 
                                                                ++ (show $ makeCHProg (translationSet!!ti) piprog) 
                                                                ++ "\n and it is"
                                                                ++ (if may then " may convergent     " 
                                                                           else " not may convergent ") 
                                                                ++ "and" 
                                                                ++ (if should then " is should convergent" else " not should convergent")
                                                                        | (ti,pn,(may,should)) <-  con, let (piprog,maypi,shouldpi) = (piProg!!pn)   ]                                                   
                                                   mapM_ output [show i ++ ": " ++ ((\(a,b,c) -> showPiProgram a ++ (if b then " may-convergent" else " not may-convergent") ++ "/" ++ (if c then "should-convergent" else "not should-convergent")) (piProg!!i)) | i <- (sort $ nub (map snd3 con ))]
                                               output ("\nUsed counterexamples: " ++ show (sort $ nub (map snd3 con )))                                                   
                                               if not (null skipSet) then 
                                                 do
                                                  output ("\nSkipped translations (by skipset): " ++ show skipSet )
                                                  output (unlines [show i ++ ": " ++ show (translationSet!!i) | i <- skipSet])
                                                else  return ()
                                               if null sk then return () else 
                                                 do
                                                     output ("\nSkipped translations (by timeout): " ++ show sk )
                                                     output (unlines [show i ++ ": " ++ show (translationSet!!i) | i <- sk])
                                               output ("Number of candidates: " ++ (show $ length translationSet))
                                                                                    
            | otherwise        = do 
                    output ("> testing translation candidate " ++ show i ++ " :")
                    output (show $ translationSet!!i)
                    loop2 c i 0
          loop2 c i j 
            | j >= (length piProg) = do 
                                       (con,sk) <- readIORef c
                                       writeIORef c (con,sk++[i]) 
                                       output ">>> failed!"
                                       loop c (i+1)
            | otherwise =  do
                            output (">>> testing pi program " ++ show j)
                            output (show $ piProg!!j)
                            let res = checkProgramsParam [piProg!!j] [translationSet!!i]
                            maybeTimeout <- timeout timeOutVal (evaluate res)
                            case maybeTimeout of
                                 Nothing ->  output "timeout (skipped)" >> loop2 c i (j+1)
                                 Just _ -> if null res then do 
                                                             (con,sk) <- readIORef c 
                                                             let may =  isMayConvergent (evaluateProgram $ makeCHProg (translationSet!!i) (fst3 (piProg!!j)) )
                                                             let should =  isShouldConvergent (evaluateProgram $ makeCHProg (translationSet!!i) (fst3 (piProg!!j)) )
                                                             writeIORef c (insert (i,j,(may,should)) con,sk)
                                                             output "ok" 
                                                             loop c (i+1) 
                                                       else loop2 c i (j+1)

fst3 (a,b,c) =a
snd3 (a,b,c) =b
trd3 (a,b,c) =c

checkTestSetParam testS xs = 
    map  (\trans -> map (\(prog,m,s) -> (makeCHProg trans prog,m,s))  testS) xs

checkProgramsParam testS ps = filter checkConvergenceTransAll (checkTestSetParam testS ps)
