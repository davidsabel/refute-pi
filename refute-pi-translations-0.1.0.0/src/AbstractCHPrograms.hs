module AbstractCHPrograms(
  checkConvergencies -- :: Program -> (IsMayConvergent,IsShouldConvergent)
 ,Action(..)
 ,AbstractCHProcess
 ,AbstractCHProgram
 ,IsMayConvergent    -- synonymous to Bool
 ,IsShouldConvergent -- synonymous to Bool
 ,evaluateProgram
 ,isShouldConvergent
 ,isMayConvergent
 ,evalActionParStep
 ,evalActionParSteps
 ,initStore
 ,showState
   ) where

import Data.List
import qualified Data.Map as Map

initStore = Map.empty
type Offset = Int

data Action = PutName SharedName SharedName 
            | TakeName SharedName SharedName Offset
            | PutUnit SharedName SingleName
            | TakeUnit SharedName SingleName
            | CHStop   
         deriving (Eq)
showState mp = 
    intercalate "\t" [k' ++ ":" ++ v' | (k,v) <- Map.assocs mp, let v' = case v of {Just w -> show w; Nothing -> "-"}, let k' = showK k]
        
showK s = 
 let (a,b) =     break (== '.') s
 in a ++ [if not (null b) then last b else  ' ']
instance Show Action where
    show (PutName x y)    = "putS_"   ++  x ++ " " ++ y
    show (TakeName x y i) = "takeS_"  ++  x ++ " " ++ y 
    show (PutUnit x c)    = "putCh_"  ++  x ++ " " ++ [last c]     
    show (TakeUnit x c)   = "takeCh_" ++  x ++ " " ++ [last c]     
    show CHStop = "stop"
type SingleName = String
type SharedName = String
type AbstractCHProcess = [Action]
type AbstractCHProgram = [[Action]]
        
type IsMayConvergent = Bool
type IsShouldConvergent = Bool

type Store        =  (Map.Map String (Maybe String))
type Environment  =  (Map.Map String String)
type Thread = ([Action])


updateNames 0 sub val xs = map (replace sub val) xs
updateNames i sub val (x:xs) = x:(updateNames (i-1) sub val xs)

replace sub val (PutName var1 var2) = PutName (if var1 == sub then val else var1) (if var2 == sub then val else var2)
replace sub val (TakeName var1 var2 i) = TakeName (if var1 == sub then val else var1) var2 i
replace sub val (PutUnit sharedvar pvar) = 
     PutUnit (if sharedvar == sub then val else sharedvar) pvar
replace sub val (TakeUnit sharedvar pvar) = 
     TakeUnit (if sharedvar == sub then val else sharedvar) pvar
replace sub val (CHStop) = CHStop

     
     
evalAction :: Thread -> Store -> Maybe (Thread,Store)
evalAction ((PutName var val):xs) store =
  case Map.lookup var store of 
       Nothing -> let newStore = storeInsert var val store  in  Just (xs,newStore)
       Just value  -> case value of 
                        Nothing -> let newStore = storeInsert var val store  in  Just (xs,newStore)
                        Just _ -> Nothing                                    -- MVar is filled, blocked
 
evalAction ((TakeName var sub offset):xs) store =
 let 
 in case Map.lookup var store of
       Nothing -> Nothing -- MVar is not existent: Is like empty MVar
       Just value  ->  case value of 
                            Nothing -> Nothing -- MVar is empty, blocked
                            Just val -> let 
                                         newStore = Map.insert var Nothing store
                                        in Just (updateNames offset sub val xs,newStore)



                                        
evalAction ((PutUnit sharedvar pvar):xs) store   =
  case Map.lookup (hierarchicalName sharedvar pvar) store of 
       Nothing -> let newStore = storeInsert (hierarchicalName sharedvar pvar) "Unit" store  
                  in  Just (xs,newStore)
       Just value  -> case value of 
                        Nothing -> let newStore = storeInsert (hierarchicalName sharedvar pvar) "Unit" store  
                                   in  Just (xs,newStore)
                        Just _ -> Nothing                       -- MVar is filled, blocked
 
evalAction ((TakeUnit sharedvar pvar):xs) store =
  case Map.lookup (hierarchicalName sharedvar pvar) store of
       Nothing -> Nothing -- MVar is not existent: Is like empty MVar
       Just value ->  case value of 
                            Nothing -> Nothing -- MVar is empty, blocked
                            Just "Unit" -> let 
                                            newStore = Map.insert (hierarchicalName sharedvar pvar) Nothing store
                                           in Just (xs,newStore)
                                        
evalAction (CHStop:_) _ = Nothing
evalAction []  _ = Nothing

hierarchicalName share private = share ++ "." ++ private

storeInsert :: String -> String -> Store -> Store                  
storeInsert var val store = Map.insert var (Just val) store
 
evalActionParStep :: [Thread] -> Store -> [([Thread],Store)]        
evalActionParStep [] store = []
evalActionParStep (([]):xxs) store =        
      [(([]):yys, store'') | 
                        (yys,store'') <- (evalActionParStep xxs store)]
evalActionParStep ((x:xs):xxs) store   
 | Just (xs',store') <- evalAction (x:xs) store = 
     ((xs'):xxs,store'):[((x:xs):yys, store'') | 
                        (yys,store'') <- (evalActionParStep xxs store)]
                    
 | Nothing <- evalAction (x:xs) store =
       [((x:xs):yys, store'') | 
                        (yys,store'') <- (evalActionParStep xxs store)]
                        
evalActionParSteps [] = []
evalActionParSteps  ((proc,store):rest) = 
    case evalActionParStep proc store of
      [] -> (proc,store):(evalActionParSteps rest)
      next -> evalActionParSteps (next ++ rest)
      
evaluateProgram :: AbstractCHProgram -> [([Thread],Store)]      
evaluateProgram proc = evalActionParSteps [(proc,(Map.empty))]

stopped :: AbstractCHProcess -> Bool
stopped (CHStop:_)      = True 
stopped _ = False
successful :: AbstractCHProgram -> Bool
successful = any stopped
isMayConvergent :: [([Thread],Store)] -> Bool
isMayConvergent    xs = (any successful (map fst xs))
isShouldConvergent :: [([Thread],Store)] -> Bool
isShouldConvergent xs = let ys = map fst xs in 
                             (any successful ys) && (all successful ys)

                             

checkConvergencies :: AbstractCHProgram -> (IsMayConvergent,IsShouldConvergent)
checkConvergencies p = 
   let res = evaluateProgram p
   in (isMayConvergent res,isShouldConvergent  res)

   
   
   
