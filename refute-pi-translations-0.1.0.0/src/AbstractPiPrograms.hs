module AbstractPiPrograms(
  PiProg(..)
 ,PiProcess
 ,PiProgram
 ,showPiProcess
 ,showPiProgram
 ,testSet
 ,testConsistency -- checks whether the True/False-value sin the testSet are correct
 ,calcOrig
 ,testSetGen
  ,checkTest
   ) where


import qualified Data.Map as Map
import Data.List
    
data PiProg = Output String String | Input String String Int | Stop
 deriving(Show,Eq)

showPiProg (Output x y) = concat [x,"!",y]
showPiProg (Input x y _)  = concat [x,"?",y] 
showPiProg Stop = "Stop"
 
type PiProcess = [PiProg]
type PiProgram = [PiProcess]

showPiProcess :: PiProcess -> String
showPiProcess = intercalate "." . map showPiProg
showPiProgram :: PiProgram -> String
showPiProgram = intercalate "|" . map showPiProcess


testConsistency = and $ map checkTest testSet

data RealPiProg  = Inputxy String String RealPiProg | Outputxy String String RealPiProg | Null | PiStop
 deriving(Eq,Show)

type RealPiProgram = [RealPiProg]

interaction (Inputxy x1 y1 p1) (Outputxy x2 y2 p2) 
    | x1 == x2 = [subs y1 y2 p1, p2]
interaction (Outputxy x2 y2 p2) (Inputxy x1 y1 p1)
    | x1 == x2 = [subs y1 y2 p1, p2]
interaction _ _ = []    

normalize xs = let r = filter (/= Null) xs  in if null r then [Null] else (if PiStop `elem` r then  [PiStop] else r)

     
allS xs = [let (a,r:b) = splitAt i xs in (r,a++b) | i <- [0..(length xs)-1]]
  
doubleS xs = [(p,q,map snd zs) | (r@(j,p),ys) <- allS (zip [1..] xs)
                       , (s@(i,q),zs) <- allS ys, j < i]

oneStep xs =  case [(res++r) | (p,q,r) <- doubleS xs, let res = (interaction p q), not (null res)] of 
                [] -> Nothing
                x -> Just x

exec x = execIt [x]
execIt :: [[RealPiProg]] -> [[RealPiProg]]
execIt xs = if all (==Nothing) (map oneStep xs) then xs else  execIt (concatMap nextStep xs)
 

nextStep s = case oneStep s of 
              Just  r -> r 
              Nothing -> [s]



maycon = any (any (== PiStop)) 
shouldcon = all (any (== PiStop)) 
 
subs x y PiStop = PiStop    
subs x y Null = Null
subs x y (Inputxy a b p) 
    | x == a    = Inputxy y b (subs x y p)
    | otherwise = Inputxy a b (subs x y p)

subs x y (Outputxy a b p) = Outputxy (if x== a then y else a) (if x==b then y else b) (subs x y p)



   
 -- hab Take erweitern muessen um dritten Parameter:  Anzahl der actions, in denen nicht ersetzt wird

calcOrig p = let res = exec ( translateProg  p ) 
             in (maycon res, shouldcon res)
   
translate ((Output x y):rest) = Outputxy x y (translate rest)
translate ((Input x y n):rest) = Inputxy x y (translate rest)
translate [Stop] = PiStop
translate [] = Null
translateProg xs = map translate xs
translateTest (p,b1,b2) = (translateProg p,b1,b2)
translatePruefProgs = map translateTest testSet 
checkTest orig =      let (p,b1,b2) = translateTest orig 
                          r = exec p
                          p1 = maycon r 
                          p2 = shouldcon r
                      in if b1 == p1 then 
                            if p2 == b2 then True 
                                        else error ("shouldcon error: " ++ show orig ++ "\n\n translates to: " ++ show p ++ "\n\n" ++ show r)
                            else error ("maycon error: " ++ show orig ++ "\n\n translates to: " ++ show p ++ "\n\n" ++ show r)    
 

testSet = [([[Output "x" "y",Input "x" "y" 0,Stop]],False,False),
             ([[Output "x" "y",Stop], [Input "x" "y" 0]],True,True),
             ([[Output "x" "y"], [Input "x" "y" 0,Stop]],True,True),
      --         ([[Output "x" "y"], [Input "x" "y" 0,Stop],[Output "xx" "yy"], [Input "xx" "yy" 0]],True,True),
             ([[Input "x" "y" 0,Stop]],False,False),
             ([[Output "x" "y",Stop]],False,False),
             ([[Output "x" "y",Stop],[Output "xx"  "yy"],[Input "x" "y" 0]],True,True),
              ([[Output "x" "y"],[Output "xx"  "yy",Stop],[Input "x" "y" 0]],False,False),
             ([[Output "x" "y",Stop],[Input "x" "y" 0],[Input "xxx" "yyyy" 0]],True,True),
             --- P =  ('xy.x(z).stop | x(w).0)
               ([[Output "x" "y", Input "x" "z" 0, Stop],  [Input "x" "w" 0]],False,False),
               --Neu wegen TT:  
               --- P =  ('xy.'xz.stop | x(w).0)  und symmetrische
               ([[Output "x" "y", Output "x" "z", Stop],  [Input "x" "w" 0]],False,False),
        --       ([[Output "x" "y", Output "x" "z", Output "z" "zz", Stop],  [Input "x" "w" 0,Input "x" "w" 0]],False,False),
               ([[Input "x" "y" 0, Input "x" "z" 0, Stop],  [Output "x" "w", Input "w" "z" 0]],False,False),
             --- P =  ('xz.'za.stop | 'xw.'wa.stop | x(y).y(_).0)
             ([[Output "x" "z", Output "z" "a", Stop], 
               [Output "x" "w", Output "w" "a", Stop],
               [Input "x" "y" 0, Input "y" "u" 0]],True,True),
                 --  P =  ('xz.'za |  x(w).w(b).stop | x(y).'xy.0)
              ([[Output "x" "z", Output "z" "a"], 
               [Input "x" "w" 0, Input "w" "b" 0, Stop],
               [Input "x" "y" 0, Output "x" "y"]],True,True),
               --- P =  x(z). z(a).Stop | (x(w).w(a)a.Stop | 'x(y).'y(b).0)
              ([[Input "x" "z" 0, Input "z" "a" 0, Stop], 
               [Input "x" "w" 0, Input "w" "a" 0, Stop],
               [Output "x" "y", Output "y" "u"]],True,True) ,
               --  neue Test ans Ende, damit es nicht so lange dauert
                       ([[Output "x" "y"], [Input "x" "y" 0,Stop], [Output "x" "y"]],True,True),
                      ([[Output "x" "y",Output "x" "y"], [Input "x" "y" 0,Stop], [Input "x" "y" 0, Output "x" "y"]],True,True),
                       ([[Output "x" "y"], [Input "x" "y" 0], [Output "x" "y",Stop]],True,False),
                --  P =  ('xy.'xz.'zu. stop | x(w).0 | x(u).0 | 'xu.0)   und symmetrische  
                --  das ist das  --22er das zu lange dauert
               ([[Output "x" "y", Output "x" "z", Output "z" "u", Stop],  [Input "x" "w" 0], [Input "x" "ww" 0], [Output "x" "u"]],False,False),
                 ([[Input "x" "y" 0, Input "x" "z" 0, Input "z" "u" 0, Stop],  [Output "x" "w"], [Output "x" "ww"], [Input "x" "u" 0]],False,False), 
                 ([[Input "x" "y" 0, Input "x" "z" 0, Input "z" "u" 0, Stop],  [Output "x" "w"], [Output "x" "ww"], [Input "x" "u" 0],[Input "x" "u" 0]],False,False), 
                 ([[Input "x" "y" 0, Input "x" "z" 0, Input "z" "u" 0, Stop],  [Output "x" "w"], [Output "x" "ww"], [Output "x" "u"]],False,False),
                 ([[Input "x" "y" 0, Input "x" "z" 0, Input "z" "u" 0, Stop],  [Output "x" "w"], [Output "x" "ww"], [Output "w" "u"]],True,False),  
                 --  verschiedene Wege zum Stop:
                  ([[Input "x" "y" 0, Input "y" "z" 0, Stop],  [Output "x" "w"], [Output "x" "ww"], [Output "w" "u"],[Output "ww" "u"]],True,True),  
                 --  hier zwei Tuerme die sich gegenseitig abarbeiten
             ([[Output "x" "y", Input "y" "z" 0, Stop],  [Input "x" "w" 0, Output "w" "u" ]],True,True),
              ([[Output "x" "y", Input "y" "z" 0, Input "z" "zz" 0],  [Input "x" "w" 0, Output "w" "u" ],[Output "u" "uu",Stop]],True,True),
               ([[Output "x" "y", Input "y" "z" 0, Input "z" "zz" 0], [Input "x" "w"  0], [Input "x" "w" 0, Output "w" "u" ],[Output "u" "uu",Stop]],True,False),
               -- 'xw.x(y).y'(q).0 | x(z).0 | x'(z).0 | w(p).Stop   --  von David
               --   neu dazu:  weiss nicht ob die sinnvoll sind ---
           ([[Output "x" "y"],[Input "x" "y" 0,Stop],[Input "x" "y" 0,Stop]],True,True),
            ([[Output "x" "y",Stop],[Output "x" "y",Stop],[Input "x" "y" 0]],True,True),
             ([[Output "x" "y"],[Output "x" "y"],[Input "x" "y" 0,Stop],[Input "x" "y" 0,Stop]],True,True),
            ([[Output "x" "y",Stop],[Output "x" "y",Stop],[Input "x" "y" 0],[Input "x" "y" 0]],True,True),
            ([[Output "x" "y",Stop],[Output "x" "y",Stop],[Output "x" "y",Stop],[Input "x" "y" 0]],True,True),
              ([[Output "x" "y"],[Input "x" "y" 0,Stop],[Input "x" "y" 0,Stop],[Input "x" "y" 0,Stop]],True,True),
               ([[Output "x" "y",Stop],[Output "x" "y",Stop],[Output "x" "y",Stop],[Input "x" "y" 0]],True,True)
               ,([[Output "x" "w", Input "x" "y" 0, Output "y" "q"]
                ,[Input "x" "z" 0]
                ,[Output "x" "z"]
                ,[Input "w" "p" 0, Stop]
                ],False,False) 
           
      --     fuer den letzten Fall? 
      --    'xy.x(z).'z(q) | x(z)  | x(z) | 'x(z) | y(u).Stop    
                ,([[Output "x" "y", Input "x" "z" 0,   Output "z" "q"]
                ,[Input "x" "z" 0]
                ,[Input "x" "z" 0]
                 ,[Output "x" "z"]
                 ,[Input "y" "u" 0,Stop]
                ],False,False)                
                --   symmetrische Variante: 
                ,([[Input "x" "w" 0, Output "x" "y",   Input "y" "q" 0]
                ,[Input "x" "z" 0]
                ,[Output "x" "z"]
                ,[Output "w" "p", Stop]
                ],False,False) 
                --  das dauert auch zu lange, keine AHnung ob da ne schleife ist:
                  , -- x'z.z'a.p'q.Stop | x'w.w'a.p(_).Stop | x(y).y(u).0 | x(l).l(_).0  (David: Versuch 
                   ([[Output "x" "z", Output "z" "a",Output "p" "q",Stop],  
                     [Output "x" "w", Output "w" "a",Input "p" "r" 0, Stop],
                     [Input "x" "y" 0, Input "y" "u" 0],
                     [Input "x" "l" 0, Input "l" "u" 0]
                    ],True,True)
             ]   
             
testSetGen nameset threads threadsizeAll =
 let inputoperations = [Input x y 0 | x <- nameset, y <- nameset]
     outputoperations = [Output x y |  x <- nameset, y <- nameset]
     operations = inputoperations ++ outputoperations
     final = [[Stop],[]]
     
     build ps =  let next = [ op:p | p <- ps, op <- operations]
                 in ps : (if length (head next) <= (threadsizeAll-threads+1) then (build next) else [])
--      anyThread = let x = permutations (inputoperations ++ outputoperations)
--                  in [y ++ [Stop] | y <- x] ++ x
     rek 0 sz = [[]]
     rek _ sz 
        | sz <= 0 = [[]]
     rek i sz = [t:xs | t <- anyT, length t <= sz, xs <- rek (i-1) (sz - (length t)) ]
     anyT = (concat (build final))
      
 in
     map (\x -> let (a,b) = calcOrig x in (x,a,b))  (rek threads threadsizeAll)
     
    
      
