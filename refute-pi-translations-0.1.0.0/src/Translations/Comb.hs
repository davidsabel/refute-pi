module Translations.Comb where
import Translations.Translation
import Data.List
import Data.Char

initActions = [([TrSendPut],[TrSendTake])]

isTake (TrCheckTake _) = True
isTake (TrSendTake) = True
isTake _ = False

number TrSendTake = 2
number TrSendPut  = 1
number (TrCheckTake xs) = 10+(sum $ map ord xs)
number (TrCheckPut xs)  = (10+(sum $ map ord xs))*(-1)

cmp (x,y) (x',y') = compare (map number x,map number y) (map number x',map number y')

fastNub (x:y:xs)
    | x == y = fastNub (x:xs)
    | otherwise = x:(fastNub (y:xs))
fastNub x = x
distributeActions actions = map (\t -> renameTT t) (distributeActions' actions initActions)
distributeActions' [] current = current
distributeActions' (a:actions) current =
 distributeActions' actions 
      (nubBy isRenamingPair $ fastNub $ sortBy cmp $ concat [([(s,receive) | s <- insertEverywhere a send]
            ++ [(send,r) | r <- insertEverywhere a receive])
           |  (send,receive) <- current
           ])

insertEverywhere :: a -> [a] -> [[a]]
insertEverywhere i [] = [[i]]   
insertEverywhere i (a:as) = 
      
      (i:a:as):[a:xs | xs <- (insertEverywhere i as)]
            
            
   {-

distributeActions  actions  = 
     nubBy isRenamingPair [(s,r) | (send,receive) <- (distributeActNoPerm  actions initActions)
                                 , s <- nubBy isRenaming (permutations send)
                                 , r <- (permutations receive)]-}

                                   
distributeActNoPerm [] current = current
distributeActNoPerm (a:actions) current = 
   concat [ [(a:send,receive),(send,a:receive)] | (send,receive) <- distributeActNoPerm actions current ]

distributeActSymm uses [] current = current
distributeActSymm uses (a:b:actions) current = 
   concat [[((replicate uses a)++send,(replicate uses b)++receive),((replicate uses b)++send,(replicate uses a)++receive)] | (send,receive) <- distributeActSymm uses actions current ]
   
distributeActionsSymm  uses actions  = map renameTT $
     nubBy isRenamingPair 
         [(s,r) | (send,receive) <- (distributeActSymm uses actions initActions)
                , s <- nubBy isRenaming (permutations send)
                , r <-  (permutations receive) 
         ] 
   
distributeActionsWeighted  actions  = map renameTT $
     nubBy isRenamingPair 
         [(s,r) | (send,receive) <- (distributeActNoPerm  actions initActions)
                , length send == (1+length receive) || (length receive == (1+length send)) || (length send == length receive)
                , s <- nubBy isRenaming (permutations send)
                , r <-  (permutations receive) 
         ] 

         
         
         
         
-- faster?         
         
distributeActionsWeighted'  actions  = 
     nubBy isRenamingPair 
         [(s,r) | (send,receive) <- (distributeActNoPermW (1+((length actions) `div` 2)) actions initActions)
                , s <- nubBy isRenaming  (permutations send)
                , r <- (permutations receive) 
         ] 
         
distributeActNoPermW len [] current = current
distributeActNoPermW len acts@(a:actions) current = 
   concat [ if (length send) >= len then [(send,a:receive)]
                                    else if (length receive) >= len then [(a:send,receive)]
                                                                    else [(a:send,receive),(send,a:receive)]
       
                 | (send,receive) <- distributeActNoPermW len actions current ]


renameTT t =  rename t [] [show i | i <- [1..]]
    
renameT t names  = rename t [] names
                 
rename :: ([Translation],[Translation]) -> [(String,String)] -> [String] -> ([Translation],[Translation])
rename (((TrCheckTake x):xs),u) rlist names
          | Just z <- lookup x rlist = let (a,b)= (rename (xs,u) rlist names) in ((TrCheckTake z):a,b)
          | Nothing <- lookup x rlist = let z = head names in let (a,b) = rename (xs,u)  ((x,z):rlist) (tail names) in ((TrCheckTake z):a,b)
rename (((TrCheckPut x):xs),u) rlist names
          | Just z <- lookup x rlist = let (a,b) = (rename (xs,u) rlist names) in ((TrCheckPut z):a,b)
          | Nothing <- lookup x rlist = let z = head names in let (a,b) = (rename (xs,u) ((x,z):rlist) (tail names)) in ((TrCheckPut z):a,b)
rename ((x:xs),u) rlist names = let (a,b) = (rename (xs,u) rlist names) in (x:a,b)
                                            
                                            
rename ([],((TrCheckTake x):xs)) rlist names
          | Just z <- lookup x rlist = let (a,b)= (rename ([],xs) rlist names) in (a,(TrCheckTake z):b)
          | Nothing <- lookup x rlist = let z = head names in let (a,b) = rename ([],xs)  ((x,z):rlist) (tail names) in (a,(TrCheckTake z):b)
rename ([],((TrCheckPut x):xs)) rlist names
          | Just z <- lookup x rlist = let (a,b)= (rename ([],xs) rlist names) in (a,(TrCheckPut z):b)
          | Nothing <- lookup x rlist = let z = head names in let (a,b) = rename ([],xs)  ((x,z):rlist) (tail names) in (a,(TrCheckPut z):b)
rename ([],(x:xs)) rlist names = let (a,b) = (rename ([],xs) rlist names) in (a,x:b)
rename ([],[]) rlist rnames = ([],[])
     
    
isRenaming p1 p2 
    | (length p1) /= length (p2) = False
isRenaming p1 p2 =
     rename p1 [] nameList == rename p2 [] nameList
    where
        nameList = ["_i" ++ show i | i <- [1..]]
        rename :: [Translation] -> [(String,String)] -> [String] -> [Translation]
        rename ((TrCheckTake x):xs) rlist names
          | Just z <- lookup x rlist = (TrCheckTake z):(rename xs rlist names)
          | Nothing <- lookup x rlist = let z = head names in (TrCheckTake z):(rename xs  ((x,z):rlist) (tail names))
        rename ((TrCheckPut x):xs) rlist names
          | Just z <- lookup x rlist = (TrCheckPut z):(rename xs rlist names)
          | Nothing <- lookup x rlist = let z = head names in (TrCheckPut z):(rename xs ((x,z):rlist) (tail names))
        rename (x:xs) rlist names = x:(rename xs rlist names)
        rename [] rlist rnames = []

isRenamingPair (p1,q1) (p2,q2) 
    | length p1 /= length p2 = False
    | length q1 /= length q2 = False
isRenamingPair (p1,q1) (p2,q2) =
     rename (p1,q1) [] nameList == rename (p2,q2) [] nameList
    where
        nameList = ["_i" ++ show i | i <- [1..]]
        rename :: ([Translation],[Translation]) -> [(String,String)] -> [String] -> ([Translation],[Translation])
        rename (((TrCheckTake x):xs),u) rlist names
          | Just z <- lookup x rlist = let (a,b)= (rename (xs,u) rlist names) in ((TrCheckTake z):a,b)
          | Nothing <- lookup x rlist = let z = head names in let (a,b) = rename (xs,u)  ((x,z):rlist) (tail names) in ((TrCheckTake z):a,b)
        rename (((TrCheckPut x):xs),u) rlist names
          | Just z <- lookup x rlist = let (a,b) = (rename (xs,u) rlist names) in ((TrCheckPut z):a,b)
          | Nothing <- lookup x rlist = let z = head names in let (a,b) = (rename (xs,u) ((x,z):rlist) (tail names)) in ((TrCheckPut z):a,b)
        rename ((x:xs),u) rlist names = let (a,b) = (rename (xs,u) rlist names) in (x:a,b)
                                            
                                            
        rename ([],((TrCheckTake x):xs)) rlist names
          | Just z <- lookup x rlist = let (a,b)= (rename ([],xs) rlist names) in (a,(TrCheckTake z):b)
          | Nothing <- lookup x rlist = let z = head names in let (a,b) = rename ([],xs)  ((x,z):rlist) (tail names) in (a,(TrCheckTake z):b)
        rename ([],((TrCheckPut x):xs)) rlist names
          | Just z <- lookup x rlist = let (a,b)= (rename ([],xs) rlist names) in (a,(TrCheckPut z):b)
          | Nothing <- lookup x rlist = let z = head names in let (a,b) = rename ([],xs)  ((x,z):rlist) (tail names) in (a,(TrCheckPut z):b)
        rename ([],(x:xs)) rlist names = let (a,b) = (rename ([],xs) rlist names) in (a,x:b)
        rename ([],[]) rlist rnames = ([],[])
     

allTwoTransActionsSend    =  [TrSendPut,  TrSendTake] 
allTwoTransActionsCheckA  =  [TrCheckTake "A",  TrCheckPut "A"] 
allTwoTransActions        =  allTwoTransActionsSend ++ allTwoTransActionsCheckA
allTwoSTransActions       =  TrSendPut: allTwoTransActionsCheckA
allTwoRTransActions       =  TrSendTake : allTwoTransActionsCheckA

    

all2CombIt [] = [([TrSendPut],[TrSendTake])]
all2CombIt ((x,y):actions)
 = nub (concat [[(sort $ x:send,y:receive),(sort $ y:send,x:receive)] | (send,receive) <- (all2CombIt actions)])
     
all2CombCmds actions = 
     let xs = all2CombIt actions 
     in nub [(a',b') |  (a,b) <- xs , a' <- nub (permutations a), b' <- nub (permutations b)]
     
 
 
all2Comb2Cmds = [([a,b],[c,d]) | a <- allTwoTransActions
               , b <- allTwoTransActions
               , c <- allTwoTransActions
               , d <- allTwoTransActions
               , a == TrSendPut || b == TrSendPut
               , c == TrSendTake || d == TrSendTake
               , sort [a,b,c,d] == sort allTwoTransActions]
all2Comb3Cmds = [([a,b,c],[d,e,f]) | a <- (allTwoTransActions++allTwoTransActionsCheckA)
               , b <- (allTwoTransActions ++ allTwoTransActionsCheckA)
               , c <- (allTwoTransActions ++ allTwoTransActionsCheckA)
               , d <- (allTwoTransActions ++ allTwoTransActionsCheckA)
               , e <- (allTwoTransActions ++ allTwoTransActionsCheckA)
               , f <- (allTwoTransActions ++ allTwoTransActionsCheckA)
               , a == TrSendPut || b == TrSendPut || c == TrSendPut
               , d == TrSendTake || e == TrSendTake || f == TrSendTake
               , sort [a,b,c,d,e,f] == sort (allTwoTransActions  ++ allTwoTransActionsCheckA)]
all2Comb4Cmds = nub [([a,b,c,d],[e,f,g,h]) | a <- (allTwoTransActions++allTwoTransActionsCheckA++allTwoTransActionsCheckA)
               , b <- (allTwoTransActions ++ allTwoTransActionsCheckA++allTwoTransActionsCheckA)
               , c <- (allTwoTransActions ++ allTwoTransActionsCheckA++allTwoTransActionsCheckA)
               , d <- (allTwoTransActions ++ allTwoTransActionsCheckA++allTwoTransActionsCheckA)
               , e <- (allTwoTransActions ++ allTwoTransActionsCheckA++allTwoTransActionsCheckA)
               , f <- (allTwoTransActions ++ allTwoTransActionsCheckA++allTwoTransActionsCheckA)
               , g <- (allTwoTransActions ++ allTwoTransActionsCheckA++allTwoTransActionsCheckA)
               , h <- (allTwoTransActions ++ allTwoTransActionsCheckA++allTwoTransActionsCheckA)
               , a == TrSendPut || b == TrSendPut || c == TrSendPut || d == TrSendPut
               , e == TrSendTake || f == TrSendTake || g == TrSendTake || h == TrSendTake  
               , sort [a,b,c,d,e,f,g,h] == sort (allTwoTransActions  ++ allTwoTransActionsCheckA++ allTwoTransActionsCheckA)]

    
main2Cmds = checkPrograms all2Comb2Cmds    
    
genericCalc numchk numAccessPerChk =
 let allps = all2CombCmds (concat [(replicate numAccessPerChk (TrCheckTake x,TrCheckPut x))
                                    |                                       x <- take numchk (map (\x -> [x]) ['A'..'Z'])])
 in allps
 
-- main1 = do
--         [n,m] <- getArgs
--         genericCheck (read n) (read m)
genericCheck numchk numAccessPerChk =
 let allps = all2CombCmds (concat [(replicate numAccessPerChk (TrCheckTake x,TrCheckPut x)) | 
                                              x <- take numchk (map (\x -> [x]) ['A'..'Z'])])
 in do
     print (length allps)
     print (checkPrograms allps)

    
